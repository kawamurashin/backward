package 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import view.ViewManager;
	
	/**
	 * ...
	 * @author jaiko
	 */
	public class Main extends Sprite 
	{
		
		public function Main():void 
		{
			if (stage) init();
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event = null):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			// entry point
			layout();
		}
		
		private function layout():void 
		{
			var viewManager:ViewManager = new ViewManager();
			addChild(viewManager);
		}
		
	}
	
}