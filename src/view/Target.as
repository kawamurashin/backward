package view 
{
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author jaiko
	 */
	public class Target extends Sprite 
	{
		private var sprite:Sprite;
		
		public function Target() 
		{
			super();
			if (stage) init(null);
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			//
			layout();
		}
		
		private function layout():void 
		{

			var g:Graphics;
			//
			sprite = new Sprite();
			addChild(sprite);
			g = sprite.graphics;
			g.beginFill(0xFF0000);
			g.drawCircle(0, 0, 5);
			
			this.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
		}
		
		private function mouseDownHandler(e:MouseEvent):void 
		{
			this.startDrag();
			stage.addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);
		}
		
		private function mouseUpHandler(e:MouseEvent):void 
		{
			this.stopDrag();
		}
		
	}

}