package view 
{
	import flash.display.Sprite;
	import flash.events.Event;
	import flash.events.MouseEvent;
	
	/**
	 * ...
	 * @author jaiko
	 */
	public class ViewManager extends BallManager
	{
		
		public function ViewManager() 
		{
			super();
			if (stage) init(null);
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			//
			layout();
		}
		
		private function layout():void 
		{
			var i:uint;
			var n:uint;
			var target:Target;
			trace("getge");
			targetList = [];
			n = 2;
			for (i = 0; i < n; i++)
			{
				target = new Target();
				addChild(target);
				target.x = 100 + 300 * i;
				target.y = 200 - 50 * i;
				targetList.push(target);
			}
			
			stage.addEventListener(MouseEvent.CLICK, clickHanlder);
		}
		
		private function clickHanlder(e:MouseEvent):void 
		{
			ballAdd();
		}
		
	}

}