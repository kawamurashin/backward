package view 
{
	import flash.display.Graphics;
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author jaiko
	 */
	public class Ball extends Sprite 
	{
		public var vx:Number = 0;
		public var vy:Number = 0;
		public function Ball() 
		{
			super();
			if (stage) init(null);
			else addEventListener(Event.ADDED_TO_STAGE, init);
		}
		
		private function init(e:Event):void 
		{
			removeEventListener(Event.ADDED_TO_STAGE, init);
			//
			layout();
		}
		
		private function layout():void 
		{
			var g:Graphics;
			var sprite:Sprite = new Sprite();
			addChild(sprite);
			g = sprite.graphics;
			g.beginFill(0x000000);
			g.drawCircle(0, 0, 2);
		}
		
	}

}