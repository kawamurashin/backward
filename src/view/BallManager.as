package view 
{
	import flash.display.Sprite;
	import flash.events.Event;
	
	/**
	 * ...
	 * @author jaiko
	 */
	public class BallManager extends Manager 
	{
		private var ballList:Array = [];
		public function BallManager() 
		{
			super();
			addEventListener(Event.ENTER_FRAME, enterFrameHandler);
		}
		

		
		protected function ballAdd():void
		{
			var ball:Ball = new Ball();
			addChild(ball);
			var startTarget:Target = targetList[0];
			ball.x = startTarget.x;
			ball.y = startTarget.y;
			
			
			var l:Number =  targetList[1].x - targetList[0].x;
			var h0:Number = Math.abs(targetList[1].y - targetList[0].y);
			var dy:Number =  100;
			var h:Number = h0 + dy;
			var t:Number = (Math.sqrt(2 * gravity * dy ) + Math.sqrt(2 * gravity * h)) / gravity;
			var v_s:Number = Math.sqrt(Math.pow((1 / t), 2) + 2 * gravity * dy);
			var theta_s:Number = -1* Math.atan(( t * Math.sqrt(2 * gravity * dy)) / l); 
			
			
			
			var v:Number = v_s;
			
			ball.vx = v * Math.cos(theta_s);
			ball.vy = v * Math.sin(theta_s);
			
			ballList.push(ball);
			
		}
		private function enterFrameHandler(e:Event):void 
		{
			var i:uint;
			var n:uint;
			var ball:Ball;
			var ax:Number;
			var ay:Number;
			n = ballList.length;
			for (i = 0; i < n; i++)
			{
				ball = ballList[i];
				ax = 0;
				ay = gravity;
				ball.vx += ax;
				ball.vy += ay;
				ball.x += ball.vx;
				ball.y += ball.vy;
				
			}
			
		}
	}

}